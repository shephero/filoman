extern crate walkdir;
extern crate kiss3d;
extern crate nalgebra as na;

use na::Vec3;
use kiss3d::window::Window;
use kiss3d::light::Light;


use std::fs;
use walkdir::WalkDir;

fn directory_contents(directory: &str) {
    fs::read_dir(directory).unwrap();
}

fn main() {
    println!("Hello World!");
    println!("Working Directory: ");
    for entry in WalkDir::new(".") {
        let entry = entry.unwrap();
        println!("{}", entry.path().display());
    }
}
